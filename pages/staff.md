---
title: Meet the App Lab Staff!
stem: staff
---

<div class="grid-column-layout">

<div class="box">
<img class="headshot" src="/jeff.jpg" alt="jeff"/>

### Jeff Terrell

- Computer Science Professor of the Practice

I hail from Texas and Arkansas, although I've been in this area of NC since I came here (to UNC-CS) for grad school in 2004. I've been a tech entrepreneur and "software craftsman" since I graduated in 2009, and I'm excited to share the software skills I've learned with you all in the App Lab. I have a lot of experience with web apps (and a little less with mobile apps), so I can help with many different kinds of things. In my spare time, I enjoy hanging out with my family (including my 3 young kiddos), reading (usually something Christian/spiritual or SF), watching movies, and occasionally playing StarCraft 2.
</div>

<div class="box">
<img class="headshot" src="/daniel.jpg" alt="daniel"/>

### Daniel Manila

- Junior
- Computer Science

I'm from Durham, North Carolina. I like reading books, playing games (especially strategy board games), and figuring out how to fix the newest problem with my Linux system. I enjoy solving new problems, so please throw any questions you have at me and I'll do my best to help.
</div>


<div class="box">
<img class="headshot" src="/isha.jpg" alt="isha"/>

### Isha Kabra

- Senior
- Computer Science major

I'm from Cary, North Carolina and I love henna designing, playing tennis, and playing the harmonium (an Indian Classical instrument that I have a Bachelor's Degree for!). I have experience with native Android mobile app development and am very excited to contribute to projects and help other students build applications as an App Lab Manager!
</div>

<div class="box">
<img class="headshot" src="/rohaid.png" alt="rohaid"/>

### Rohaid Bakheet

- Senior
- Computer Science major

I'm from Greensboro, North Carolina and like to draw, edit videos, and watch cartoons. I also like to play and build videogames, and I have built experience with using Unity, Javascript, C#, HTML, etc. I'm always eager to learn about new things and would love to help out anyone starting on a game or web based project! 
  
</div>


<div class="box">
<img class="headshot" src="/lasya.jpeg" alt="lasya"/>

### Lasya Pullakhandam

- Junior
- Computer Science major, Entrepreneurship minor

I'm from Cary, North Carolina. I enjoy listening to podcasts, reading books, and travelling. I have experience using HTML&CSS, React, and Swift to build mobile and web applications. I am super excited to be part of the App Lab and help students in whichever way I can!
  
</div>

<div class="box">
<img class="headshot" src="/jonah.png" alt="jonah"/>

### Jonah Soberano

- Senior
- Computer Science & Statistics major, Neuroscience minor

I’m from Chapel Hill, NC (never left my whole life), and I enjoy playing Scrabble, hiking, surfing, and getting lost down rabbit holes on YouTube. I have experience in web development with JavaScript, HTML, CSS, PHP5, and in app development with React Native and Flutter/Dart. I also love challenging myself and learning, so I’m excited to learn more from this experience, start new projects, and help out as best as I can.
</div>

<h1 class="span-grid">Volunteers</h1>

<div class="box">
<img class="headshot" src="/nolan.jpg" alt="nolan"/>

### Nolan Scobie

- Senior
- Computer Science major

I'm from Asheville, NC. I like cooking, making things, and going to the river with my dog. I am mostly volunteering to help with Unity and AR/VR development, but am happy to try and help with anything!
</div>

<div class="box">
<img class="headshot" src="/jiayixu.jpg" alt="jiayi"/>

### Jiayi Xu

- Junior
- Computer Science & Psychology

I am from Shanghai, China. I am a volunteer of AR/VR help session at APP Lab and outreach lead of CARVR. I have experience in VR development with Unity and Unreal, and mobile AR app with various tool kits. I am familiar with C#, Java, C++ and Python. I like reading, calligraphy, and travelling.
</div>

<div class="box">
<img class="headshot" src="/shaik.jpg" alt="shaik"/>

### Husam Shaik

I am from Charlotte, NC and like to spend my time exploring a lot of hobbies from reading and video games to painting and making weird apps. I am also very involved in the AR/VR and am excited to see how our lives will change as the technology becomes more main stream. Looking forward to seeing you at the App Lab !!!
</div>

</div>

<h1>Alumni</h1>
- Grant Miller
- [Brandon Shearin](https://www.linkedin.com/in/brandon-shearin/)
- [Manuel Cabrejos](https://www.linkedin.com/in/mcabrejost)
- Mary Luong
- [Anna Truelove](https://www.linkedin.com/in/anna-truelove/)