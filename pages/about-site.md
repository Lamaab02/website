---
title: About this website
stem: about-site
---

I (Jeff) have been a bit annoyed at [how bloated modern web sites
are](https://idlewords.com/talks/website_obesity.htm). Sometimes the heft is
justified, but often it isn't. In response, this site is an attempt at something
lighter, partially inspired by (language warning) [this
website](http://bettermotherfuckingwebsite.com/).

I like the idea of static site generators (SSGs). Most web sites don't need to
be dynamic. (One might argue that web sites, as opposed to web apps, don't need
to be dynamic by definition.) So let me write my content in a familiar format
like Markdown, then compile the content into a web-site-in-a-directory and push
the directory up to a server. I struggled to find an SSG I liked, then finally,
perhaps suffering from an acute case of [yak
shaving](https://en.wiktionary.org/wiki/yak_shaving), I decided to write my own.

You can see the beautiful [Clojure](https://clojure.org/) code in [the GitLab
repo](https://gitlab.com/unc-app-lab/website). The tests run in about 3 seconds.
The site builds, from scratch, in about 3 seconds as well. (That's longer than
I'd like, but not too painful.)

When I push a new commit to the `master` branch on GitLab, an integration kicks
in and deploys the site to [Netlify](https://www.netlify.com/), which provides
hosting of static sites for free (even including a custom domain name, purchased
separately).

So here we are. No mobile-optimized page (but your phone renders everything just
fine, I bet). [A web bloat
score](https://www.webbloatscore.com/?url=https://applab.unc.edu/) under 1.0.
Some fairly light [CSS
stats](https://cssstats.com/stats?url=https://applab.unc.edu&ua=Browser
Default). And a sub-second load time, even on a 3G network.
