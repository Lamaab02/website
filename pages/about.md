---
title: About the UNC App Lab
stem: about
---

The UNC App Lab was started in the fall of 2018 by [Dr. Jeff
Terrell](https://terrell.web.unc.edu), UNC Computer Science Professor of the
Practice. Its mission is:

> To teach students how to build web and mobile applications, in service to the
> university community.

The App Lab is usually located in Sitterson Hall, room 027, but due to COVID, we 
are operating virtually. Join us on Zoom through [this link](https://unc.zoom.us/j/93881281276?pwd=OWlWTS8zOFJoZmhwazlsc1JNa1hYQT09). The App Lab is open weekday afternoons for 
the spring 2021 semester; you can [view the full schedule](/calendar/).

Here's a picture of the App Lab. You can also watch [a video
walkthrough](https://youtu.be/7adkR-PplEA).

<a href="/applab-photo.jpeg">
<img src="/applab-photo.jpeg" width="600" height="450"
     alt="a photo of the UNC App Lab" />
</a>

For the 2020-2021 academic year, we are working on a web app to improve error 
messages in the Clojure programming language. The app is called [Clem: the Clojure 
Error Mediator](https://gitlab.com/unc-app-lab/clem). This semester (spring 2021),
people are also working on a project to help the App Lab track and visualize 
visitor data. More information on this Check-In app can be accessed 
[here](https://github.com/UNC-App-Lab/Check-In). Another project that is currently 
being undertaken is [Rhythme](https://gitlab.com/unc-app-lab/Rhyth-Me), a 2D platform 
game developed using Unity and C#. Dr. Terrell is always collecting ideas for 
future semesters, and students are welcome to work on their own ideas as well.

Dr. Terrell manages projects much like students will encounter in industry as
professional software engineers. There is a Slack group and a project management
board (using Trello) with specific tasks defined. Code is reviewed before being
accepted. Students can learn not only how apps are built, but also how software
is built in a professional, collaborative environment.

In keeping with the Carolina spirit, collaboration is emphasized in the App Lab.
Pair programming in particular is a great way to collaborate. In many cases it's
faster than programming alone, and it usually results in more learning and
skills transfer than almost any other activity.

The lab is equipped with equipment designed to encourage collaboration.
There are 3 electric desks, wide enough for two people, that can be transitioned
between sitting and standing heights. Each desk has a nice monitor attached, so
that two people can share a screen. The chairs are comfortable, highly
adjustable, and new. There is also a larger table for conversations or as
overflow seating. And there's a treadmill desk, if you want to work and walk at
the same time.

If you're a student, you can learn [how to get involved](/get-involved/) in
the App Lab.

The App Lab is also run by 5 App Lab managers. To meet the staff click [here](/staff/).

You can also [sign up for the App Lab newsletter](/newsletter-sign-up/).

Anybody is welcome to email us at [applab@unc.edu](mailto:applab@unc.edu) with
further questions about the App Lab.
