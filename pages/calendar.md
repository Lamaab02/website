---
title: Calendar
stem: calendar
---
<div>
<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FNew_York&amp;src=Y3MudW5jLmVkdV90azQyY3JoamM3dDY1MXFkdXJnMnNmMTk4b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Y3MudW5jLmVkdV84cGNoaGhycGc0Nm92ajJkanF1aGI0cjI3MEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%230B8043&amp;color=%23EF6C00&amp;mode=WEEK&amp;showPrint=0&amp;showTabs=1&amp;showTz=0" style="border-width:0" width="650" height="600" frameborder="0" scrolling="no"></iframe></div>

The calendar above shows our current hours for the upcoming week. During those
hours we are open for any kind of help for any skill level. From debugging, to
planning an app, to learning a new language&mdash;we're here to help.

As of Monday, August 30, 2021, the physical App Lab in Sitterson Hall room
SN027 is open again!

Per UNC community standards, masks are required at all times.

### The Virtual App Lab

At times, we may need to shift to a virtual App Lab instead of an in-person
one. The calendar above will note if that happens. In that case, you can
instead join us on Zoom at this link:
[https://unc.zoom.us/j/93881281276?pwd=OWlWTS8zOFJoZmhwazlsc1JNa1hYQT09](https://unc.zoom.us/j/93881281276?pwd=OWlWTS8zOFJoZmhwazlsc1JNa1hYQT09).
When you do so please check in using this link:
[http://app-lab-check-in.herokuapp.com/](http://app-lab-check-in.herokuapp.com/).
There will be somebody waiting on the other end to help or work with. And you
don't have to worry about distracting others with your question, because others
can go program or discuss their work in a breakout room, separate from the main
room.

Also, [our Slack group](https://join.slack.com/t/unc-app-lab/signup) is a good
way to stay in touch with everything and everybody in the App Lab.

Lastly, check out [this guide for pair programming with
Zoom](https://www.youtube.com/watch?v=L-THvCKl2DA).
