---
title: A Redux.js tutorial
author: Jeff Terrell
date: 2019-02-01
tags: tutorial, redux, javascript, functional programming, state, effects
---

A couple of days ago, I gave a lecture to COMP 523 (software engineering laboratory) about Redux.js. I developed a simple example based on the (perhaps [familiar](https://applab.unc.edu/posts/2018/10/02/building-tic-tac-toe-mobile-app-react-native/)) tic-tac-toe idea. Then I built an app based on Redux one step at a time to demonstrate its key concepts.

<!--more-->

Why Redux? Because I believe Redux apps are simpler, and that simplicity makes it easier to create robust apps without bugs. It's my opinion that most of the bugs and challenges in programming are incidental, based on the choice of language and approach, rather than essential and inherent in the problem being solved, and that most of that incidental complexity stems from an uncareful approach to effects: the parts of your code that actually change some state somewhere. Indeed, the object oriented style favored by many is usually (though not always) uncareful in exactly this way: effects constitute a rather large proportion of the total lines of code. In Redux, there is exactly one way to create an effect: by dispatching one of a predefined set of actions against the state stored by Redux. This careful isolation and enumeration of effects, and a commensurate emphasis on pure functions (those without effect or external reference) tend to make apps written in Redux simple and robust.

You can view the code and follow along step-by-step from [the repo on GitLab](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe). You can also read [the Redux.js web site](https://redux.js.org/), which includes some pretty well written documentation.
