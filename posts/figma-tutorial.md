---
title: A design tutorial using Figma
author: Jeff Terrell
date: 2019-09-10
tags: tutorial, design, introduction, figma
---

A few days ago, with the help of my TA, Srihari Pratapa, I gave [a tutorial on
using Figma](../../../../../figma-tutorial.pdf) for UI design to my [COMP
523](https://comp523.cs.unc.edu) class.

<!--more-->

For people with an idea for a web app or mobile app (or people trying to
implement somebody else's idea), the first thing I recommend is to create a
design of what the screens of the app will look like when it's finished. This
accomplishes a few things:

- It lets you iterate on your design more quickly, rather than trying to
  actually code up a full UI.
- As a consequence of the above, it helps you notice weird or unsatisfying UI
  elements while it's all still fresh in your mind. (When you have to work for
  a week or more to get a UI implemented, it's no longer new, and it's harder
  to notice weird parts because you're closer to it.)
- It's a fantastic communication device (especially if you've created not just
  designs but a clickable prototype&mdash;see below) to show people what you're
  planning to build. This can help entrepreneurs find funding and developers
  find teammates.

I mentioned the term &ldquo;clickable prototype&rdquo;.
[Figma](https://www.figma.com) makes it easy not only to create the designs for
the screens, but also to connect them together so that clicking on an element
in one screen (e.g. a button) switches to another screen that you designed. In
that way, it actually feels much like you're interacting with a full blown app,
even though it's a constrained and canned experience. The tutorial includes a
clickable prototype.

When I gave this tutorial to my class, most people finished it, without help,
in about 30&ndash;45 minutes. That includes getting started with Figma, which
is a free web app that requires no installation of software. By the end, you'll
have a clickable prototype of a simple calculator app.

[Here's the tutorial again.](../../../../../figma-tutorial.pdf) Enjoy!

If you're wanting to build a web app or mobile app and need help creating a
design, feel free to swing by the App Lab [when it's open](/calendar/).

