---
title: What is Pair Programming?
author: Rohaid Bakheet
date: 2021-03-29
tags: introduction, pair programming, design, howto, resources
---

So you're working on a project with another person or group of people for the first time, and you're unsure of how to stay motivated or stay focused, what on earth do you do? Well there are multiple solutions to said issue, but we'll be talking about a specific technique called pair programming. 

<!--more-->

### What is Pair Programming?
    
Pair programming is the practice of two people coding together. One person (the driver) will type the code, while another person (the navigator) will review that piece of code and give feedback or tips. In-person pair programming is done with coders side by side, but it can also be done virtually through screensharing and video calls. This method of coding allows for less bugs, transfer of information and ideas between coders, and can allow for more focus.	


### Tips for Pair Progamming 
- The person not coding should always be analyzing and thinking of where the code will be used, what is the final strategy, etc. 
- For beginners, 15 minutes of pair programming is recommended before taking a break. More experienced coders can go about 45 minutes till a break. 
- If you have concerns with the code, bring them up as soon as possible. 
- Make sure to switch! One person should not be driving the entire time, and it’s helpful to get different perspectives.
- Make sure to clearly communicate and bring up when you are feeling tired or need to take a break. A tired programmer can lead to more bugs than fixes.
- If you run into a bug that neither of you can explain, seek a third person who isn't involved in the project and explain what you're doing.
- Don't blame all the errors on one person, own up to your mistakes together.
- You should always view yourself and your partner as equals, even if one has more experience than the other. The point of pair programming is to constantly bring in new inputs and experiences so that everyone learns something. Don't look down on an inexperienced coder and don't put the more experienced coder on a pedestal.
- Try to be open minded when your partner brings up changes.
- Clearly verbalize what you're coding and explain your thought process.
- Be patient with your teammate and take time to learn how the other works. Being a successful pair programmer takes time.
- Most importantly, celebrate your successes together!


### Additional Resources 

If you want to learn more, here are some resources:

 - [A research paper that details various pair programming tips](https://collaboration.csc.ncsu.edu/laurie/Papers/Kindergarten.PDF) 
 - [A thorough blog post on methods/benefits/and downfalls of pair programming](https://martinfowler.com/articles/on-pair-programming.html)
 - [A video on pair programming with Zoom by Professor Terrell](https://www.youtube.com/watch?v=L-THvCKl2DA)

